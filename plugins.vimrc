""""""""""""""""""
" Plugin Manager "
"    vim-plug    "
""""""""""""""""""
"" exeptional option placement
" ale conflicts with neomake linter, which has been disabled
" ale does not know this and would throw an exception if not
" for the comment below
let g:ale_emit_conflict_warnings = 0

"""""""""""
" Plugins "
"""""""""""

" Instructions from https://github.com/junegunn/vim-plug
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
call plug#begin('~/.local/share/nvim/plugged')
" Plug 'author/name'
"
" GUI
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter' " Indicate modifications next to linenumbers

" Utilities
Plug 'ntpeters/vim-better-whitespace'
Plug 'tpope/vim-surround'
Plug 'easymotion/vim-easymotion'
Plug '907th/vim-auto-save'
Plug 'morhetz/gruvbox' " colourscheme
Plug 'jiangmiao/auto-pairs' " auto creates closing ) }

" Programming
Plug 'w0rp/ale' " linter if neomake does not convince
Plug 'Valloric/YouCompleteMe' " if deoplete does not convince
Plug 'neomake/neomake'
Plug 'tpope/vim-fugitive'
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' } " autocomplete
" Plug 'Shougo/echodoc.vim'
" Python
" Plug 'fisadev/vim-isort'
" Plug 'google/yapf', { 'rtp': 'plugins/vim', 'for': 'python' } " automatic code formatting
" Plug 'zchee/deoplete-jedi' " deoplete source for python, see requirements on github
"
" C++
Plug 'octol/vim-cpp-enhanced-highlight'
call plug#end()

"""""""""""""""""
" Configuration "
"""""""""""""""""

""""""""""""""""""""""""""""""
" Vim Cpp Enhanced Highlight "
""""""""""""""""""""""""""""""
let g:cpp_class_scope_highlight = 1

" """""""
" " Ale "
" """""""
" " Bind F8 to fixing problems with ALE
" nmap <F8> <Plug>(ale_fix)
" " run linters when leaving insert mode
" "let g:ale_lint_on_insert_leave
"
" let g:ale_python_flake8_executable = 'python3'
" let g:ale_python_flake8_options = '-m flake8'
"
" let g:ale_python_pylint_executable = 'python3'
" let g:ale_python_pylint_options = '-rcfile /usr/bin/pylint.rc'
"
" " quickly jump between warnings and errors
" nmap <silent> <C-k> <Plug>(ale_previous_wrap)
" nmap <silent> <C-j> <Plug>(ale_next_wrap)

"""""""""""""""""
" vim-auto-save "
"""""""""""""""""
let g:auto_save = 1 " enable AutoSave on Vim startup
let g:auto_sav_in_insert_mode = 1 " Enable AutoSave in Insert Mode
let g:auto_save_silent = 1  " do not display the auto-save notification

"""""""""""""""""""""""""
" vim-better-whitespace "
"""""""""""""""""""""""""
"let g:CurrentLineWhitespaceOff soft " don't mark bad whitespace in active line
let g:better_whitespace_enable = 1
let g:strip_whitespace_on_save = 1
let g:strip_whitespace_confirm = 0
let g:show_spaces_that_precede_tabs = 1

" """"""""""""
" " deoplete "
" """"""""""""
" let g:deoplete#enable_at_startup = 1
" " deoplete tab-complete
" inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
" inoremap <expr><S-tab> pumvisible() ? "\<c-p>" : "\<tab>"
" autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" """"""""
" " yapf "
" """"""""
" autocmd FileType python nnoremap <leader>y :0,$!yapf<Cr><C-o>
" noremap <F3> :call yapf#YAPF()<cr>

"""""""""""
" neomake "
"""""""""""
" When writing a buffer.
"call neomake#configure#automake('w')
let g:neomake_python_enabled_makers = [] " disable linting and let ale handle that

let g:neomake_python_flake8_maker = {
    \ 'args': ['--ignore=E221,E241,E272,E251,W702,E203,E201,E202',  '--format=default'],
    \ 'errorformat':
        \ '%E%f:%l: could not compile,%-Z%p^,' .
        \ '%A%f:%l:%c: %t%n %m,' .
        \ '%A%f:%l: %t%n %m,' .
        \ '%-G%.%#',
    \ }
let g:neomake_python_enabled_makers = ['flake8']
