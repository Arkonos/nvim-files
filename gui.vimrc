"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" colorscheme firewatch
" colorscheme mustang
" colorscheme slate
let g:gruvbox_italic=1
colorscheme gruvbox
set background=dark

" Set utf8 as standard encoding
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Highlight active line
set cursorline
hi CursorLine term=NONE cterm=NONE ctermbg=black


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set 7 lines to the cursor - when moving vertically using j/k
set so=10000

" Turn on the WiLd menu
set wildmenu
set wildmode=list:longest:full

" Ignore compiled files
set wildignore=*.n,*~,*.pyc

" Always show line numbers
set relativenumber
set number

" Height of the command bar
set cmdheight=2

" A buffer becomes hidden when it is abandoned
set hid

" Arrow keys and h, l go the the next/previous line after reaching end/start of current line
set whichwrap+=<,>,h,l

" When searching try to be smart about cases
set smartcase

" Highlight search results
"set hlsearch

" Incremental searches will be shown during typing
set incsearch

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Don't conceal common words
set conceallevel=0
let g:tex_conceal = ""


""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l

""""""""""""""""""""
" => Column width
""""""""""""""""""""
autocmd ColorScheme * highlight Bang ctermfg=7
" Python
au BufWinEnter *.py call matchadd('Bang', '\%>80v.\+', -1)
" C++
au BufWinEnter *.cpp call matchadd('Bang', '\%>80v.\+', -1)
au BufWinEnter *.h call matchadd('Bang', '\%>80v.\+', -1)
au BufWinEnter *.hpp call matchadd('Bang', '\%>80v.\+', -1)

"""""""""""""
" Splitting "
"""""""""""""
" Easier split navigation
" ctrl-j instead of ctrl-w + j
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" More natural split opening
set splitbelow
set splitright
